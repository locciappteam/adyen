﻿using Adyen.Core.Import;
using System.Threading.Tasks;

namespace Adyen.APP
{
    class Program
    {
        static void Main(string[] args)
        {
            Task.Run(async () =>
            {
                //await Import.Setup();
               await Import.PagamentosStatus(@"\\amsaocts01\Interfaces\Import\Brazil\Boletos\", @"\\amsaocts01\Interfaces\Import\Brazil\Boletos\Backup\", "Adyen");
            }).Wait();

        }        
    }
}