﻿using Adyen.Data.Context;
using Adyen.Data.Model;
using Adyen.Data.Model.SettlementDetailReport.Excel;
using Adyen.Data.Repositories;
using EFCore.BulkExtensions;
using Loccitane.Interfaces;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Adyen.Core.Import
{
    public class Import
    {
        public static async Task OrdersCSV(string fileNomenclature, string sourcePath, string backupPath)
        {
            CultureInfo thisCulture = new CultureInfo("pt-BR", false);
            NumberStyles style = NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands;

            Log.Write(String.Format($"{DateTime.Now} : \n INÍCIO Importação \n "));
            string[] fileList = Directory.GetFiles(sourcePath, string.Format("*{0}*.csv", fileNomenclature));

            Log.Write(String.Format($"{DateTime.Now} : \n Encontrado(s) {fileList.Count()} Arquivo(s) \n "));

            if (fileList.Any())
            {
                Log.Write(String.Format($"{DateTime.Now} : \n Início da formatação dos registros do Arquivo \n "));

                foreach (var file in fileList)
                {
                    var filename = file.Substring(sourcePath.Length);
                    filename = filename.Replace("\\", "");

                    string[] contentFile = File.ReadLines(file, Encoding.GetEncoding("iso-8859-1")).Skip(1).ToArray();
                    List<settlement_detail_report_batch> ListaOrders = new List<settlement_detail_report_batch>();

                    foreach (var line in contentFile)
                    {
                        try
                        {
                            settlement_detail_report_batch _settlement_detail_report_batch = new settlement_detail_report_batch();

                            _settlement_detail_report_batch.COMPANYACCOUNT = line.Split(',')[0];
                            _settlement_detail_report_batch.MERCHANTACCOUNT = line.Split(',')[1];
                            _settlement_detail_report_batch.PSPREFERENCE = Convert.ToInt64(line.Split(',')[2]);
                            _settlement_detail_report_batch.MERCHANTREFERENCE = line.Split(',')[3];
                            _settlement_detail_report_batch.PAYMENTMETHOD = line.Split(',')[4];
                            _settlement_detail_report_batch.CREATIONDATE = Convert.ToDateTime(line.Split(',')[5]);
                            _settlement_detail_report_batch.TIMEZONE = line.Split(',')[6];
                            _settlement_detail_report_batch.TYPE = line.Split(',')[7];
                            _settlement_detail_report_batch.MODIFICATIONREFERENCE = line.Split(',')[8];
                            _settlement_detail_report_batch.GROSSCURRENCY = line.Split(',')[9];
                            _settlement_detail_report_batch.GROSSDEBIT = line.Split(',')[10] == "" ? 0 : Decimal.Parse(line.Split(',')[10].Replace('.',','), style, thisCulture);
                            _settlement_detail_report_batch.GROSSCREDIT = line.Split(',')[11] == "" ? 0 : Decimal.Parse(line.Split(',')[11].Replace('.', ','), style, thisCulture);
                            _settlement_detail_report_batch.EXCHANGERATE = line.Split(',')[12] == "" ? 0 : Convert.ToInt32(line.Split(',')[12]);
                            _settlement_detail_report_batch.NETCURRENCY = line.Split(',')[13];
                            _settlement_detail_report_batch.NETDEBIT = line.Split(',')[14] == "" ? 0 : Decimal.Parse(line.Split(',')[14].Replace('.', ','), style, thisCulture);
                            _settlement_detail_report_batch.NETCREDIT = line.Split(',')[15] == "" ? 0 : Decimal.Parse(line.Split(',')[15].Replace('.', ','), style, thisCulture);
                            _settlement_detail_report_batch.COMMISSION = line.Split(',')[16] == "" ? 0 : Decimal.Parse(line.Split(',')[16].Replace('.', ','), style, thisCulture);
                            _settlement_detail_report_batch.MARKUP = line.Split(',')[17] == "" ? 0 : Decimal.Parse(line.Split(',')[17].Replace('.', ','), style, thisCulture);
                            _settlement_detail_report_batch.SCHEMEFEES = line.Split(',')[18] == "" ? 0 : Decimal.Parse(line.Split(',')[18].Replace('.', ','), style, thisCulture);
                            _settlement_detail_report_batch.INTERCHANGE = line.Split(',')[19] == "" ? 0 : Decimal.Parse(line.Split(',')[19].Replace('.', ','), style, thisCulture);
                            _settlement_detail_report_batch.ADVANCED = line.Split(',')[20];
                            _settlement_detail_report_batch.ADVANCEMENTCODE = line.Split(',')[21];
                            _settlement_detail_report_batch.ADVANCEMENTBATCH = line.Split(',')[22];
                            _settlement_detail_report_batch.PAYMENTMETHODVARIANT = line.Split(',')[23];
                            _settlement_detail_report_batch.BATCHNUMBER = line.Split(',')[24];
                            _settlement_detail_report_batch.RESERVED4 = line.Split(',')[25];
                            _settlement_detail_report_batch.RESERVED5 = line.Split(',')[26];
                            _settlement_detail_report_batch.RESERVED6 = line.Split(',')[27];
                            _settlement_detail_report_batch.RESERVED7 = line.Split(',')[28];
                            _settlement_detail_report_batch.RESERVED8 = line.Split(',')[29];
                            _settlement_detail_report_batch.RESERVED9 = line.Split(',')[30];
                            _settlement_detail_report_batch.RESERVED10 = line.Split(',')[31];
                            _settlement_detail_report_batch.ACQUIRER = line.Split(',')[32];
                            _settlement_detail_report_batch.MODIFICATIONMERCHANTREFERENCE = line.Split(',')[33];
                            _settlement_detail_report_batch.BOOKINGDATE = Convert.ToDateTime(line.Split(',')[34]);
                            _settlement_detail_report_batch.BOOKINGDATETIMEZONE = line.Split(',')[35];
                            _settlement_detail_report_batch.ADDITIONALTYPE = line.Split(',')[36];
                            _settlement_detail_report_batch.INSTALLMENTS = line.Split(',')[37];
                            _settlement_detail_report_batch.ISSUERCOUNTRY = line.Split(',')[38];
                            _settlement_detail_report_batch.SHOPPERCOUNTRY = line.Split(',')[39];
                            _settlement_detail_report_batch.MERCHANTORDERREFERENCE = line.Split(',')[40];
                            _settlement_detail_report_batch.BOOKINGTYPE = line.Split(',')[41];

                            ListaOrders.Add(_settlement_detail_report_batch);

                        }
                        catch (Exception ex)
                        {
                            Log.Write(String.Format($"{DateTime.Now} : \n Erro ao processar linha : {line.Split(',')[2]} \n Erro: {ex.InnerException}"));
                            continue;
                        }
                    }
                    Log.Write(String.Format($"{DateTime.Now} : \n Fim da formatação dos registros do Arquivo : {filename} \n"));

                    List<ADYEN_SETTLEMENT_DETAIL> lista = new List<ADYEN_SETTLEMENT_DETAIL>();
                    using (var _ctx = new ContextAdyen())
                    {
                        Log.Write(String.Format($"{DateTime.Now} : \n Início da importação do arquivo : {filename} \n"));

                        lista = (
                                    from tbl in ListaOrders.Distinct()
                                    select new ADYEN_SETTLEMENT_DETAIL
                                    {
                                        COMPANYACCOUNT = tbl.COMPANYACCOUNT,
                                        MERCHANTACCOUNT = tbl.MERCHANTACCOUNT,
                                        PSPREFERENCE= tbl.PSPREFERENCE,
                                        MERCHANTREFERENCE= tbl.MERCHANTREFERENCE,
                                        PAYMENTMETHOD= tbl.PAYMENTMETHOD,
                                        CREATIONDATE= tbl.CREATIONDATE,
                                        TIMEZONE= tbl.TIMEZONE,
                                        TYPE= tbl.TYPE,
                                        MODIFICATIONREFERENCE= tbl.MODIFICATIONREFERENCE,
                                        GROSSCURRENCY= tbl.GROSSCURRENCY,
                                        GROSSDEBIT= tbl.GROSSDEBIT,
                                        GROSSCREDIT= tbl.GROSSCREDIT,
                                        EXCHANGERATE= tbl.EXCHANGERATE,
                                        NETCURRENCY  = tbl.NETCURRENCY,
                                        NETDEBIT     = tbl.NETDEBIT,
                                        NETCREDIT    = tbl.NETCREDIT,
                                        COMMISSION    = tbl.COMMISSION,
                                        MARKUP= tbl.MARKUP,
                                        SCHEMEFEES = tbl.SCHEMEFEES,
                                        INTERCHANGE = tbl.INTERCHANGE,
                                        ADVANCED = tbl.ADVANCED,
                                        ADVANCEMENTCODE= tbl.ADVANCEMENTCODE,
                                        ADVANCEMENTBATCH= tbl.ADVANCEMENTBATCH,
                                        PAYMENTMETHODVARIANT= tbl.PAYMENTMETHODVARIANT,
                                        BATCHNUMBER= tbl.BATCHNUMBER,
                                        RESERVED4= tbl.RESERVED4,
                                        RESERVED5 = tbl.RESERVED5,
                                        RESERVED6 = tbl.RESERVED6,
                                        RESERVED7 = tbl.RESERVED7,
                                        RESERVED8 = tbl.RESERVED8,
                                        RESERVED9 = tbl.RESERVED9,
                                        RESERVED10= tbl.RESERVED10,
                                        ACQUIRER = tbl.ACQUIRER,
                                        MODIFICATIONMERCHANTREFERENCE= tbl.MODIFICATIONMERCHANTREFERENCE,
                                        BOOKINGDATE= tbl.BOOKINGDATE,
                                        BOOKINGDATETIMEZONE= tbl.BOOKINGDATETIMEZONE,
                                        ADDITIONALTYPE= tbl.ADDITIONALTYPE,
                                        INSTALLMENTS= tbl.INSTALLMENTS,
                                        ISSUERCOUNTRY= tbl.ISSUERCOUNTRY,
                                        SHOPPERCOUNTRY= tbl.SHOPPERCOUNTRY,
                                        MERCHANTORDERREFERENCE= tbl.MERCHANTORDERREFERENCE,
                                        BOOKINGTYPE= tbl.BOOKINGTYPE
                                    }
                                ).ToList();

                         using (var transaction = await _ctx.Database.BeginTransactionAsync())
                        {
                            try
                            {
                                _ctx.BulkInsertOrUpdate(lista, options => options.BatchSize = 1000);
                                await transaction.CommitAsync();
                            }
                            catch (Exception ex)
                            {
                                await transaction.RollbackAsync();
                                Log.Write(String.Format($"{DateTime.Now} : \n Erro ao importar Arquivo {filename} Erro: {ex}\n"));
                            }
                        }
                    }
                    File.Move(file, backupPath + "Success_" + DateTime.Now.ToString("yyyy_MM_dd_HHmmss") + "_" + filename);
                    Log.Write(String.Format($"{DateTime.Now} : \n Arquivo importado com sucesso. Movido para pasta de backup!\n"));
                }
            }
            Log.Write(String.Format($"{DateTime.Now} : \n FIM Importação  \n "));
        }
        public static bool Download(string filePath, string merchantAccount, string batch, string data)
        {
            try
            {
                using (var client = new WebClient())
                {

                    client.Credentials = new NetworkCredential("report_383226@Company.LOccitane", "Wagner@123");

                    string arquivo = string.Format("settlement_detail_report_batch_{0}_{1}.csv", batch, data);
                    string merchant = string.Format("https://ca-live.adyen.com/reports/download/MerchantAccount/{0}/{1}", merchantAccount, arquivo);

                    Uri uri = new Uri(merchant);

                    string file = arquivo.ToString().Substring(arquivo.IndexOf("batch_"));
                    //Console.WriteLine("Downloading:" + file);
                    client.DownloadFile(uri, filePath + merchantAccount + "_" + file);

                }

                return true;
            }
            catch
            {
                return false;
            }
        }
        public static void Importacao(string dirLocal, string dirBackup)
        {
            Task.Run(async () =>
            {
                Console.WriteLine("== INÍCIO DA IMPORTAÇÃO ==");
                Log.Write(String.Format($"{DateTime.Now} : \n === INÍCIO DA EXECUÇÃO === \n "));

                Console.WriteLine(".. Importando Settlement Detail");
                await Import.OrdersCSV("*batch_", dirLocal, dirBackup);

                Console.WriteLine("== FIM DA IMPORTAÇÃO: VERIFIQUE O LOG PARA VER OS DETALHES == ");
                Log.Write(String.Format($"{DateTime.Now} : \n === FIM DA EXECUÇÃO === \n "));
            }
            ).Wait();
        }
        public static async Task Setup(int amountFiles = 6)
        {
            var inter = new InterfacesLibrary("IMPORT_ADYEN_REPORTS");

            string dirLocal = (string)inter.GetConvertedParameter("SETTLEMENT_DETAILS_PATH");
            string dirBackup = (string)inter.GetConvertedParameter("SETTLEMENT_DETAILS_BACKUP_PATH");

            List<MerchantAccount> merchant = new List<MerchantAccount>();
            var param = (string)inter.GetConvertedParameter("Loccitane_AuBresil_BR_ECOM"); //last valued batch
            merchant.Add(new MerchantAccount
            {
                name = "Loccitane_AuBresil_BR_ECOM",
                lastBatch = Convert.ToInt32(param.Substring(0, param.IndexOf('_'))),
                lastDate = DateTime.ParseExact(param.Substring(param.IndexOf('_') + 1), "yyyyMMdd", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd")

            });

            param = (string)inter.GetConvertedParameter("Loccitane_BR_ECOM"); //last valued batch
            merchant.Add(new MerchantAccount
            {
                name = "Loccitane_BR_ECOM",
                lastBatch = Convert.ToInt32(param.Substring(0, param.IndexOf('_'))),
                lastDate = DateTime.ParseExact(param.Substring(param.IndexOf('_') + 1), "yyyyMMdd", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd")

            });

            param = (string)inter.GetConvertedParameter("Loccitane_Bresil_BR_ECOM"); //last valued batch
            merchant.Add(new MerchantAccount
            {
                name = "Loccitane_Bresil_BR_ECOM",
                lastBatch = Convert.ToInt32(param.Substring(0, param.IndexOf('_'))),
                lastDate = DateTime.ParseExact(param.Substring(param.IndexOf('_') + 1), "yyyyMMdd", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd")

            });

            foreach (var account in merchant)
            {
                var nextBatch = (int)account.lastBatch + 1;
                var nextDate = DateTime.Parse(account.lastDate);

                while (nextDate <= DateTime.Today)
                {

                    for (int i = account.lastBatch + 1; i < account.lastBatch + amountFiles; i++)
                    {
                        bool baixou;
                        nextBatch = i;

                        try
                        {
                            baixou = Import.Download(dirLocal, account.name, nextBatch.ToString(), nextDate.ToString("yyyyMMdd"));
                            if (baixou)
                            {
                                account.lastBatch = i;
                                var value = nextBatch.ToString() + "_" + nextDate.ToString("yyyyMMdd");
                                inter.EditParameterValue(6, account.name, value);
                            }
                        }
                        catch
                        {
                            continue;
                        }

                    }

                    nextDate = nextDate.AddDays(1);
                }

            }

            Importacao(dirLocal, dirBackup);
        }
        public static async Task PagamentosStatus(string caminhoArquivo, string backArquivo, string fileNomenclature)
        {
            try
            {
                Log.Write(String.Format($"{DateTime.Now} : \n INÍCIO Importação Boletos  \n "));

                CultureInfo thisCulture = new CultureInfo("pt-BR", false);
                NumberStyles style = NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands;
                string[] fileList = Directory.GetFiles(caminhoArquivo, string.Format("*{0}*.csv", fileNomenclature));

                foreach (string file in fileList)
                {
                    FileInfo fileInfo = new FileInfo(file);
                    if (fileInfo.Exists)
                    {
                        Log.Write(String.Format($"{DateTime.Now} : \n Importando dados do arquivo: {fileInfo.Name}  \n "));

                        string[] contentFile = File.ReadLines(file, Encoding.GetEncoding("iso-8859-1")).Skip(1).ToArray();
                        List<RelatorioAdyen> ListaOrders = new List<RelatorioAdyen>();

                        foreach (string line in contentFile)
                        {
                            try
                            {
                                RelatorioAdyen relatorioAdyen = new RelatorioAdyen();
                                relatorioAdyen.PSPREFERENCE = line.Split(';')[0];
                                relatorioAdyen.MERCHANTREFERENCE = line.Split(';')[1];
                                relatorioAdyen.CREATIONDATE = Convert.ToDateTime(line.Split(';')[2]);
                                relatorioAdyen.VALUE = line.Split(';')[3] == "" ? 0 : Decimal.Parse(line.Split(';')[3].Replace('.', ','), style, thisCulture);
                                relatorioAdyen.CURRENCY = line.Split(';')[4];
                                relatorioAdyen.PAYMENTMETHOD = line.Split(';')[5];
                                relatorioAdyen.STATUS = line.Split(';')[6];
                                ListaOrders.Add(relatorioAdyen);
                            }
                            catch (Exception ex)
                            {
                                Log.Write(String.Format($"{DateTime.Now} : \n Ocorreu um erro ao importar o CSV na linha: {line}  \n "));
                                continue;
                            }
                        }

                        List<ADYEN_PAGAMENTO_STATUS> listBoletos = new List<ADYEN_PAGAMENTO_STATUS>();

                        foreach (RelatorioAdyen adyen in ListaOrders)
                        {
                            try
                            {
                                ADYEN_PAGAMENTO_STATUS boleto = new ADYEN_PAGAMENTO_STATUS();
                                boleto.PSPREFERENCE = adyen.PSPREFERENCE;
                                boleto.MERCHANTREFERENCE = adyen.MERCHANTREFERENCE;
                                boleto.CREATIONDATE = adyen.CREATIONDATE;
                                boleto.VALUE = adyen.VALUE;
                                boleto.CURRENCY = adyen.CURRENCY;
                                boleto.PAYMENTMETHOD = adyen.PAYMENTMETHOD;
                                boleto.STATUS = adyen.STATUS;
                                listBoletos.Add(boleto);
                            }
                            catch (Exception ex)
                            {
                                Log.Write(String.Format($"{DateTime.Now} : \n Ocorreu um erro ao criar lista PSPREFERENCE: {adyen.PSPREFERENCE}  \n "));
                                continue;
                            }

                        }

                        using (var _ctx = new RPT_ADYEN_PAGAMENTO_STATUS())
                        {
                            try
                            {
                                if (await _ctx.BulkingInsert(listBoletos))
                                {
                                    File.Move(file, backArquivo + "Success_" + DateTime.Now.ToString("yyyy_MM_dd_HHmmss") + "_" + fileInfo.Name);
                                    Log.Write(String.Format($"{DateTime.Now} : \n Dados salvos e arquivo movido com sucesso! \n "));
                                }
                                else
                                {
                                    throw new Exception("Ocorreu um erro ao salvar os dados.");
                                }

                            }
                            catch (Exception ex)
                            {
                                Log.Write(String.Format($"{DateTime.Now} : \n Ocorreu um erro ao salvar os Dados \n "));
                                continue;
                            }
                        }
                    }
                    else
                    {
                        Log.Write(String.Format($"{DateTime.Now} : \n Nenhum arquivo encontrado \n "));
                    }
                }
            }
            catch
            {
                Log.Write(String.Format($"{DateTime.Now} : \n OCORREU UM ERRO FATAL\n "));
            }
            finally
            {
                Log.Write(String.Format($"{DateTime.Now} : \n FIM importação de boletos \n "));
            }

        }

    }

}
