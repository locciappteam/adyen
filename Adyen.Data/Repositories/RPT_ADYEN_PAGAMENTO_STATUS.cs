﻿using Adyen.Data.Context;
using Adyen.Data.Interfaces;
using Adyen.Data.Model;

namespace Adyen.Data.Repositories
{
    public class RPT_ADYEN_PAGAMENTO_STATUS : Repository<ADYEN_PAGAMENTO_STATUS>, IRPT_ADYEN_PAGAMENTO_STATUS
    {
        public RPT_ADYEN_PAGAMENTO_STATUS() : base (new ContextAdyen() )
        {}
    }
}
