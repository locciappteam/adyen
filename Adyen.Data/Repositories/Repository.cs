﻿using Adyen.Data.Model;
using EFCore.BulkExtensions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Adyen.Data.Repositories
{
    public partial class Repository<T> : IRepository<T> where T : DefaultEntity
    {
        protected readonly DbContext _ctx;
        public Repository(DbContext context)
        {
            _ctx = context;
        }

        public void Save()
        {
            _ctx.SaveChanges();
        }

        public void Rollback()
        { }

        public T Add(T entity)
        {
            _ctx.Set<T>().Add(entity);
            Save();
            return entity;
        }

        public IEnumerable<T> AddList(IEnumerable<T> entity)
        {
            _ctx.Set<T>().AddRange(entity);
            Save();
            return entity;
        }

        public void Delete(T entity)
        {
            _ctx.Set<T>().Attach(entity);
            _ctx.Set<T>().Remove(entity);
            Save();
        }

        public void Dispose()
        {
            _ctx.Dispose();
        }

        public T Edit(T entity)
        {
            _ctx.Entry(entity).State = EntityState.Modified;
            Save();
            return entity;
        }

        public IEnumerable<T> Get()
        {
            return _ctx.Set<T>().AsNoTracking().ToList();
        }

        public T GetByID(object id)
        {
            return _ctx.Set<T>().Find(id);
        }

        public async Task<bool> BulkingInsert(List<T> listEntity, int batchSize = 10000)
        {

            bool result = true;

            using (var transaction = await _ctx.Database.BeginTransactionAsync())
            {

                try
                {
                    _ctx.BulkInsert(listEntity,options=> options.BatchSize = batchSize);
                    await transaction.CommitAsync();
                }
                catch (Exception ex)
                {
                    result = false;
                    await transaction.RollbackAsync();
                }
                finally
                {
                    transaction.Dispose();
                }

                return result;
            }
        }
    }
}
