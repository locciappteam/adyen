﻿using Adyen.Data.Model;
using Adyen.Data.Model.SettlementDetailReport;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;


namespace Adyen.Data.Context
{
    public class ContextAdyen : DbContext
    {
        public DbSet<ADYEN_SETTLEMENT_DETAIL> adyenSettlementDetails { get; set; }
        public DbSet<ADYEN_PAGAMENTO_STATUS> adyenBoletos { get; set; }

        private string _connStr { get; set; }

        public ContextAdyen(int dbMode = 1)
        {

            // HOMOLOGACAO = Conexao_DB_INTERFACES_HOM
            //PRODUCAO = Conexao_DB_INTERFACES_PROD

            switch (dbMode)
            {
                case 1:
                    _connStr = "Conexao_DB_INTERFACES_PROD";
                    break;

                case 2:
                    _connStr = "Conexao_DB_INTERFACES_HOM";
                    break;
            }
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
           .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
           .AddJsonFile("appsettings.json")
           .Build();

            optionsBuilder.UseSqlServer(configuration.GetConnectionString(_connStr));
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //ADYEN SETTLEMENT DETAIL
            modelBuilder.Entity<ADYEN_SETTLEMENT_DETAIL>().Property(id => id.ID).ValueGeneratedOnAdd();
            modelBuilder.Entity<ADYEN_SETTLEMENT_DETAIL>().HasKey(b => new { b.ID, b.PSPREFERENCE}).HasName("PK_ADYEN_SETTLEMENT_DETAIL_PSPREFERENCE");

            //ADYEN BOLETO 
            modelBuilder.Entity<ADYEN_PAGAMENTO_STATUS>().Property(id => id.ID).ValueGeneratedOnAdd();
            modelBuilder.Entity<ADYEN_PAGAMENTO_STATUS>().HasKey(id => id.ID).HasName("PK_ADYEN_BOLETOS_TST");


        }
    }
}
