﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Adyen.Data.Model
{
    [Table("ADYEN_PAGAMENTOS_STATUS")]
    public class ADYEN_PAGAMENTO_STATUS : DefaultEntity
    {

        public ADYEN_PAGAMENTO_STATUS()
        {
            IMPORTATION_DATE = DateTime.Now;
        }

        public long ID { get; set; }
        public string PSPREFERENCE { get; set; }
        public string MERCHANTREFERENCE { get; set; }
        public DateTime? CREATIONDATE { get; set; }
        public decimal? VALUE { get; set; }
        public string CURRENCY { get; set; }
        public string PAYMENTMETHOD { get; set; }
        public string STATUS { get; set; }
        public DateTime? IMPORTATION_DATE { get; set; }
    }
}
