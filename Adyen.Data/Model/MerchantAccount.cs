﻿namespace Adyen.Data.Model
{
    public class MerchantAccount
    {
        public string name { get; set; }
        public int lastBatch { get; set; }
        public string lastDate { get; set; }
    }
}
