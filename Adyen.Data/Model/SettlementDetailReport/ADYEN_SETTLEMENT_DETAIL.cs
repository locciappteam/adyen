﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Adyen.Data.Model
{
    [Table("ADYEN_SETTLEMENT_DETAIL")]
    public class ADYEN_SETTLEMENT_DETAIL
    {
        public int  ID { get; set; }
        public long PSPREFERENCE { get; set; }

        public string COMPANYACCOUNT { get; set; }

        public string MERCHANTACCOUNT { get; set; }

        public string MERCHANTREFERENCE { get; set; }

        public string PAYMENTMETHOD { get; set; }

        public DateTime? CREATIONDATE { get; set; }

        public string TIMEZONE { get; set; }

        public string TYPE { get; set; }

        public string MODIFICATIONREFERENCE { get; set; }

        public string GROSSCURRENCY { get; set; }

        public decimal? GROSSDEBIT { get; set; }

        public decimal? GROSSCREDIT { get; set; }

        public int? EXCHANGERATE { get; set; }

        public string NETCURRENCY { get; set; }

        public decimal? NETDEBIT { get; set; }

        public decimal? NETCREDIT { get; set; }

        public decimal? COMMISSION { get; set; }

        public decimal? MARKUP { get; set; }

        public decimal? SCHEMEFEES { get; set; }

        public decimal? INTERCHANGE { get; set; }

        public string ADVANCED { get; set; }

        public string ADVANCEMENTCODE { get; set; }

        public string ADVANCEMENTBATCH { get; set; }

        public string PAYMENTMETHODVARIANT { get; set; }

        public string BATCHNUMBER { get; set; }

        public string RESERVED4 { get; set; }

        public string RESERVED5 { get; set; }

        public string RESERVED6 { get; set; }

        public string RESERVED7 { get; set; }

        public string RESERVED8 { get; set; }

        public string RESERVED9 { get; set; }

        public string RESERVED10 { get; set; }

        public string ACQUIRER { get; set; }

        public string MODIFICATIONMERCHANTREFERENCE { get; set; }

        public DateTime? BOOKINGDATE { get; set; }

        public string BOOKINGDATETIMEZONE { get; set; }

        public string ADDITIONALTYPE { get; set; }

        public string INSTALLMENTS { get; set; }

        public string ISSUERCOUNTRY { get; set; }

        public string SHOPPERCOUNTRY { get; set; }

        public string MERCHANTORDERREFERENCE { get; set; }

        public string BOOKINGTYPE { get; set; }
    }
}
