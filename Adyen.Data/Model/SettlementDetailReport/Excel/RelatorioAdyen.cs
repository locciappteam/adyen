﻿using System;

namespace Adyen.Data.Model.SettlementDetailReport.Excel
{
    public class RelatorioAdyen
    {
        public string PSPREFERENCE { get; set; }
        public string MERCHANTREFERENCE { get; set; }
        public DateTime? CREATIONDATE { get; set; }
        public decimal? VALUE { get; set; }
        public string CURRENCY { get; set; }
        public string PAYMENTMETHOD { get; set; }
        public string STATUS { get; set; }
    }
}
