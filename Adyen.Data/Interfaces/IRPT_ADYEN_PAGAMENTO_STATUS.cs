﻿using Adyen.Data.Model;
using Adyen.Data.Repositories;

namespace Adyen.Data.Interfaces
{
    public interface IRPT_ADYEN_PAGAMENTO_STATUS : IRepository<ADYEN_PAGAMENTO_STATUS>
    {

    }
}
