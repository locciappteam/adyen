using Adyen.Core.Import;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Adyen.WorkerService
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private readonly IServiceScopeFactory _serviceScopeFactory;

        int amountFiles;

        private IConfiguration _configuration;
        private int _runIntervallInMinutes;

        public Worker(ILogger<Worker> logger, IServiceScopeFactory serviceScopeFactory)
        {
            _logger = logger;
            _serviceScopeFactory = serviceScopeFactory;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                _logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now);
                await Import.Setup(amountFiles);
                await Import.PagamentosStatus(@"\\amsaocts01\Interfaces\Import\Brazil\Boletos\", @"\\amsaocts01\Interfaces\Import\Brazil\Boletos\Backup\", "Adyen");
                await Task.Delay(TimeSpan.FromMinutes(_runIntervallInMinutes), stoppingToken);
            }

        }

        public override Task StartAsync(CancellationToken cancellationToken)
        {
            _configuration = _serviceScopeFactory.CreateScope().
                             ServiceProvider.GetRequiredService<IConfiguration>();
            
            _runIntervallInMinutes = int.Parse(_configuration
                                            ["App.Configurations:RunIntervalInMinutes"]);
           
            amountFiles = int.Parse(_configuration
                                            ["App.Configurations:AmountOfFiles"]);

            return base.StartAsync(cancellationToken);
        }
    }
}
